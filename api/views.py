# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from django.shortcuts import render
from rest_framework import generics
from django.http import JsonResponse
from api.models import Article
from django.core import serializers

# Create your views here.


def index(request, path=''):
    """
    The home page. This renders the container for the single-page app.
    """
    return render(request, 'index.html')


class Articles(generics.GenericAPIView):
    """
    A generic API to return articles on the basis of id or all articles
    """

    def get(self, request, article_id=None):
        """

        :param request:
        :return:
        """
        if article_id:
            article = Article.objects.get_article(article_id=article_id)
            return JsonResponse(status=200, data={
                'status': 200,
                'article': article
            })
        else:
            articles = Article.objects.get_all_articles()
            return JsonResponse(status=200, data={
                'status': 200,
                'articles': articles
            })

    def post(self, request):
        """

        :param request:
        :return:
        """
        payload = json.loads(request.body)
        try:
            Article.objects.create(**payload)
        except Exception as e:
            return JsonResponse(status=400, data={'error': str(e)})
        return JsonResponse(status=200, data={'message': 'Article created'})



