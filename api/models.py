# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core import serializers
import json

# Create your models here.


class ArticleManager(models.Manager):

	def get_article(self, article_id):
		article = list(Article.objects.filter(id=article_id).values())
		# article = serializers.serialize('json', article_object)
		return article[0]

	def get_all_articles(self):
		articles_object = list(Article.objects.all().values())
		return articles_object


class Article(models.Model):

	id = models.CharField(max_length=100, primary_key=True)
	display_name = models.CharField(max_length=120)
	url_name = models.CharField(max_length=120)
	content = models.CharField(max_length=50000)
	created_at = models.CharField(max_length=60)
	objects = ArticleManager()

	def __str__(self):
		return self.id


