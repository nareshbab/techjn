install:
	# Make script for Centos system
	bold=$(tput bold)
	normal=$(tput sgr0)

	# Creating log folder for API logs
	mkdir logs || echo "logs folder already exsist."

	#Installing python headers
	sudo yum install python-devel
	sudo easy_install pip
	sudo pip install virtualenv
	sudo yum groupinstall 'Development Tools'

	# Creating virtualenv
	virtualenv venv
	venv/bin/pip install -r requirements.txt

log-api:
	# For checking logs of API
	journalctl -f -u techjn

deploy:
	# Creating API services
	head -n 10 techjn-api.service.template > techjn-api.service
	echo "WorkingDirectory=$(PWD)" >> techjn-api.service
	echo "ExecStart=$(PWD)/venv/bin/gunicorn --workers 3 -t 300 --bind 0.0.0.0:8000 techjn.wsgi" >> techjn-api.service
	sudo mv techjn-api.service /etc/systemd/system/techjn-api.service
	sudo systemctl daemon-reload


start:
	# Starting API services
	sudo systemctl daemon-reload

	sudo systemctl start techjn-api
	sudo systemctl enable techjn-api

status:
	#Status of API service
	sudo systemctl status techjn-api

stop:
	# Stopping API services
	sudo systemctl stop techjn-api

restart:
	# Restarting API services
	sudo systemctl restart techjn-api
